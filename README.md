# mc-discord-bot

This bot will run rcon command in a minecraft server docker container.

It is compatible with [itzg/minecraft-server](https://docker-minecraft-server.readthedocs.io/en/latest/).

## Dependencies

 cargo 1.78.0 (54d8815d0 2024-03-26)
 
 rustup 1.27.1 (54dd3d00f 2024-04-24)

 ## Installation

 Once the repo is cloned.

 ```bash
 cargo install --path .
 ```

 ## Build

 Simply run 

 ```bash
 cargo build --release
 ```

 The generated binary path is `/target/release/mc-discord-bot`.

 ## Deploy

 To deploy simply build the binary for the wanted platform and then execute it.

 I personnaly run the binary using systemd service, you can find my service config file here [systemctl.example](systemctl.example).

 ## Features
  
  It is a personal project so the features are added when we really need them and when i want to add them.

  ### Whitelist

  The user can type `/whitelist` in the discord text chat and a command will display to whitelist a user onto the minecraft server.