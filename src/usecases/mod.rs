use crate::config::{self, Config};
use std::process::Command;

pub fn whitelist(mc_username: &String) -> String {
    let config: Config = config::read();

    let res = Command::new("docker")
        .args(["exec", &config.container_name])
        .arg("rcon-cli")
        .arg("whitelist add")
        .arg(mc_username)
        // .arg("-c \"rcon-cli whitelist add ianathos\"")
        .output()
        .expect("Whitelist command failed");

    if !res.stdout.is_empty() {
        String::from_utf8(res.stdout).expect("Failed getting utf-8 string")
    } else if !res.stderr.is_empty() {
        String::from_utf8(res.stderr).expect("Failed getting utf-8 string")
    } else {
        String::from("Command succeeded")
    }
}
