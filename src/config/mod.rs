use std::{env, fs, io};

const BOT_KEY: &str = "BOT_KEY";
const CONTAINER_NAME: &str = "CONTAINER_NAME";
const CONFIG_FILE_PATH: &str = "/.config/mc-discord.config";

#[derive(Debug)]
pub struct Config {
    pub bot_key: String,
    pub container_name: String,
}

pub fn setup() {
    let home_dir = env::home_dir()
        .expect("Home directory not found")
        .into_os_string()
        .into_string()
        .unwrap();
    println!("Setup config file");

    println!("What is your discord bot key ?");

    let mut bot_key = String::new();
    io::stdin()
        .read_line(&mut bot_key)
        .expect("Failed to read user input for bot_key");

    println!("What is your discord minecraft server container name?");
    let mut container_name = String::new();
    io::stdin()
        .read_line(&mut container_name)
        .expect("Failed to read user input for container_name");

    let config = format!(
        "{}={}{}={}",
        BOT_KEY, bot_key, CONTAINER_NAME, container_name
    );

    fs::write(home_dir + CONFIG_FILE_PATH, config).expect("Failed to write config file");
}

pub fn read() -> Config {
    let home_dir = env::home_dir()
        .expect("Home directory not found")
        .into_os_string()
        .into_string()
        .unwrap();
    let config_file =
        fs::read_to_string(home_dir + CONFIG_FILE_PATH).expect("Config file not found");

    let mut bot_key: &str = "";
    let mut container_name: &str = "";

    for line in config_file.lines() {
        if line.contains(BOT_KEY) {
            let splited_line: Vec<&str> = line.split('=').collect();
            bot_key = splited_line[1];
        }
        if line.contains(CONTAINER_NAME) {
            let splited_line: Vec<&str> = line.split('=').collect();
            container_name = splited_line[1];
        }
    }

    if bot_key.is_empty() {
        panic!("BOT_KEY is undefined");
    }

    if container_name.is_empty() {
        panic!("CONTAINER_NAME is undefined");
    }

    Config {
        bot_key: String::from(bot_key),
        container_name: String::from(container_name),
    }
}
