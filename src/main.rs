use std::env;

use commands::setup_poise;

mod commands;
mod config;
mod usecases;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() >= 2 {
        let arg1 = &args[1];

        if arg1 == "setup" {
            config::setup();
            return;
        }
    }

    setup_poise().await;
}
