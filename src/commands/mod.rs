use poise::serenity_prelude as serenity;

use crate::{
    config::{self, Config},
    usecases,
};

struct Data {} // User data, which is stored and accessible in all command invocations
type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

pub async fn setup_poise() {
    let config: Config = config::read();
    let intents = serenity::GatewayIntents::non_privileged();

    let commands = vec![whitelist()];
    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands,
            ..Default::default()
        })
        .setup(|ctx, _ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                Ok(Data {})
            })
        })
        .build();

    let client = serenity::ClientBuilder::new(config.bot_key, intents)
        .framework(framework)
        .await;

    client.unwrap().start().await.unwrap();
}

/// White list given minecraft Username into the server
#[poise::command(slash_command, prefix_command)]
async fn whitelist(
    ctx: Context<'_>,
    #[description = "Give your minecraft username"] mc_username: Option<String>,
) -> Result<(), Error> {
    match &mc_username {
        Some(username) => {
            println!("Processing whitelist for username {username}");
            let cmd_resp = usecases::whitelist(username);
            println!("Command whitelist responded with {cmd_resp}");
            ctx.say(cmd_resp).await?;
        }
        None => {
            ctx.say("Need a username").await?;
        }
    }

    Ok(())
}
